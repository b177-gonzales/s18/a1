let trainer = {
	Name: "Ash Ketchum",
	Age: 10,
	Pokemon: [
		'Pikachu',
		'Charizard',
		'Squirtle',
		'Bulbasaur'
	],
	friends:{
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log ("Pikachu! I choose you!")
	}
};

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.Name);
console.log("Result of square bracket notation:");
console.log(trainer['Pokemon']);
console.log('Result from talk method:');
trainer.talk();

function Pokemon (name, level)
{
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.faint = function(target){
		console.log(this.name + ' fainted');
		}
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		let newHealth = target.health - this.attack;
		console.log(target.name + "'s health is now reduced to " + newHealth);
		if (target.health - this.attack) {
			target.health = newHealth
		}
		if (newHealth <= 0) {
			target.faint();			
		}
	}

};

let pikachu = new Pokemon('Pikachu', 12);

console.log(pikachu);

let geodude = new Pokemon('Geodude', 8);

console.log(geodude);

let mewtwo = new Pokemon('Mewtwo', 100);

console.log(mewtwo);

geodude.tackle(pikachu);

console.log(pikachu);

mewtwo.tackle(geodude);

console.log(geodude);